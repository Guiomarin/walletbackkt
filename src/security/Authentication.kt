package com.uniajc.security

import com.uniajc.entity.User
import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication

val ApplicationCall.authenticateUser get() = authentication.principal<User>()