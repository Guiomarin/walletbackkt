package com.uniajc.repo

import com.uniajc.entity.Movement
import com.uniajc.entity.Wallet
import com.uniajc.models.MovementDAO
import com.uniajc.models.UserDAO
import com.uniajc.models.WalletDAO
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

class WalletRepo {

    fun getAllWallet(): List<Wallet> = transaction {
        WalletDAO.all().map { toWallet(it) }

    }

    fun getAllWalletUser(userId: Int): List<Wallet> = transaction {
        WalletDAO.find { com.uniajc.models.Wallet.user eq userId }.sortedBy { it.id }.map { toWallet(it) }
    }

    fun insertWallets(wallet: Wallet, id: Int): Wallet? {
        var createdWallet: Wallet? = null

        transaction {

            val insertWallet = WalletDAO.new {
                name = wallet.name
                lastupdate = DateTime.now()
                ammount = wallet.ammount
                user = UserDAO.get(id)
            }
            createdWallet = toWallet(insertWallet)
        }

        return createdWallet
    }


    companion object {
        fun toWallet(wallet: WalletDAO): Wallet = Wallet(
            id = wallet.id.value,
            name = wallet.name,
            lastupdate = wallet.lastupdate.toString(),
            ammount = wallet.ammount,
            user = UserRepository.toUser(wallet.user),
            movements = wallet.movements.map { toMovement(it) }
        )

        fun toMovement(movement: MovementDAO): Movement = Movement(
            id = movement.id.value,
            ammount = movement.ammount,
            concept = movement.concept,
            date = movement.date.toString(),
            image = movement.image,
            latitude = movement.latitude,
            longitude = movement.longitude
        )
    }
}