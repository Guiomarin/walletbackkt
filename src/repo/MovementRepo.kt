package com.uniajc.repo

import com.uniajc.entity.Movement
import com.uniajc.entity.Wallet
import com.uniajc.models.MovementDAO
import com.uniajc.models.WalletDAO
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

class MovementRepo {

    fun getAllMovement(): List<Movement> = transaction {
        MovementDAO.all().map { toMovement(it) }
    }

    fun insertMovements(movement: Movement): Movement? {
        var createdMovement: Movement? = null

        transaction {
            val insertMovement = MovementDAO.new {
                concept = movement.concept
                date = DateTime.now()
                ammount = movement.ammount
                image = movement.image
                latitude = movement.latitude
                longitude = movement.longitude

                wallet = WalletDAO.get(movement.wallet?.id as Int).id

                //wallet = WalletDAO.get(1).id

            }
            createdMovement = toMovement(insertMovement)
        }

        return createdMovement
    }

    companion object {
        fun toMovement(movement: MovementDAO): Movement = Movement(
            id = movement.id.value,
            concept = movement.concept,
            date = movement.date.toString(),
            ammount = movement.ammount,
            image = movement.image,
            latitude = movement.latitude,
            longitude = movement.longitude
            //,wallet = movement.wallet.id
        )


        fun toWallet(wallet: WalletDAO): Wallet = Wallet(
            id = wallet.id.value,
            name = wallet.name,
            lastupdate = wallet.lastupdate.toString(),
            ammount = wallet.ammount,
            user = UserRepository.toUser(wallet.user)
        )
    }


}