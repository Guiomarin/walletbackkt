package com.uniajc.util

import com.uniajc.models.Movements
import com.uniajc.models.Users
import com.uniajc.models.Wallet
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction


object DatabaseInit {
    fun init() {
        Database.connect(getConfigHikari())
        createTables()
    }

    private fun createTables() {
        transaction {
            SchemaUtils.create(Users, Wallet, Movements)

            if (Users.selectAll().count() == 0) {
                Users.insert {
                    it[name] = "wallet"
                    it[username] = "admin"
                    it[password] = "admin"
                }
            }

        }
    }

    private fun getConfigHikari(): HikariDataSource {
        val config = HikariConfig()
        config.dataSourceClassName = "org.postgresql.ds.PGSimpleDataSource"
        config.jdbcUrl = "jdbc:/postgres://localhost:5432/wallet"
        config.username = "wallet"
        config.password = "wallet"
        config.maximumPoolSize = 3
        config.validate()

        return HikariDataSource(config)
    }
}