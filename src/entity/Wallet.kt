package com.uniajc.entity

import org.joda.time.DateTime

data class Wallet(
    var id: Int = 0,
    var name: String = "",
    var lastupdate: String = "",
    var ammount: Long = 0,
    var user: User? = null,
    var movements: List<Movement> = emptyList()
)