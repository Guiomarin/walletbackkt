package com.uniajc.endpoints

import com.uniajc.entity.Movement
import com.uniajc.repo.MovementRepo
import com.uniajc.responses.Message
import com.uniajc.security.authenticateUser
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.MultiPartData
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.*
import kotlinx.coroutines.runBlocking
import io.ktor.request.receive

fun Route.movementEndpoint(movementRepo: MovementRepo) {
    route("movements") {
        post("create") {
            call.authenticateUser!!
            val movement = call.receive<Movement>()
            val createMovement = movementRepo.insertMovements(movement)

            if (createMovement == null) {
                call.respond(HttpStatusCode.NotAcceptable, Message("No se pudo crear el Movement"))
            } else {
                call.respond(HttpStatusCode.OK, createMovement)
            }
        }

        get {
            call.authenticateUser!!
            call.respond(HttpStatusCode.OK, movementRepo.getAllMovement())
        }
    }
}

private fun getParamsToMovement(params: MultiPartData): Movement {
    val movement = Movement()

    runBlocking {
        params.forEachPart { part ->
            when (part) {
                is PartData.FormItem -> {
                    when (part.name) {
                        "concept" -> movement.concept = part.value
                        "date" -> movement.date = part.value
                        "ammount" -> movement.ammount = part.value.toLong()
                        "image" -> movement.image = part.value
                        "latitude" -> movement.latitude = part.value.toFloat()
                        "longitude" -> movement.longitude = part.value.toFloat()
                    }
                }
            }
            part.dispose()
        }
    }

    return movement
}