package com.uniajc.endpoints

import com.uniajc.entity.Wallet
import com.uniajc.repo.WalletRepo
import com.uniajc.responses.Message
import com.uniajc.security.authenticateUser
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.MultiPartData
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.request.receive
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import kotlinx.coroutines.runBlocking

fun Route.walletEndpoint(walletRepo: WalletRepo) {

    route("wallets") {

        post("create") {
            call.authenticateUser!!
            val wallet = call.receive<Wallet>()
            var userId = call.authenticateUser!!.id;
            val createWallet = walletRepo.insertWallets(wallet, userId)

            if (createWallet == null) {
                call.respond(HttpStatusCode.NotAcceptable, Message("No se pudo crear el wallet"))
            } else {
                call.respond(HttpStatusCode.OK, createWallet)
            }
        }

        get {
            call.authenticateUser!!
            var userId = call.authenticateUser!!.id;
            //call.respond(HttpStatusCode.OK, walletRepo.getAllWallet())
            call.respond(HttpStatusCode.OK, walletRepo.getAllWalletUser(userId))

        }
    }
}
