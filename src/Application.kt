package com.uniajc

import com.uniajc.auth.JWTBase
import com.uniajc.endpoints.auth
import com.uniajc.endpoints.movementEndpoint
import com.uniajc.endpoints.userEndpoint
import com.uniajc.endpoints.walletEndpoint
import com.uniajc.repo.MovementRepo
import com.uniajc.repo.UserRepository
import com.uniajc.repo.WalletRepo
import com.uniajc.util.DatabaseInit
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.routing.*
import io.ktor.auth.*
import io.ktor.auth.jwt.jwt
import io.ktor.gson.*

fun main(args: Array<String>): Unit = io.ktor.server.tomcat.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    DatabaseInit.init()

    install(CallLogging)
    install(DefaultHeaders)
    install(AutoHeadResponse)
    install(CORS) {
        anyHost()
    }

    val userRepository = UserRepository()
    val movementRepo = MovementRepo()
    val walletRepo = WalletRepo()

    install(Authentication) {
        jwt {
            verifier(JWTBase.verifier)
            realm = JWTBase.issuer
            validate {
                it.payload.getClaim("username").asString()?.let(userRepository::findUserByUsername)
            }
        }
    }

    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }

    install(Routing) {

        auth(userRepository)

        authenticate {
            userEndpoint(userRepository)
            movementEndpoint(movementRepo)
            walletEndpoint(walletRepo)
        }
    }
}

