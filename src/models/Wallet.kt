package com.uniajc.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Wallet : IntIdTable() {
    val name = varchar("name", length = 45)
    val lastupdate = datetime("lastupdate")
    val ammount = long("ammount")
    val user = reference("user", Users)
}

class WalletDAO(id: EntityID<Int>) : IntEntity(id) { 
    companion object : IntEntityClass<WalletDAO>(Wallet)

    var name by Wallet.name
    var lastupdate by Wallet.lastupdate
    var ammount by Wallet.ammount
    var user by UserDAO referencedOn Wallet.user
    val movements by MovementDAO referrersOn Movements.wallet
}