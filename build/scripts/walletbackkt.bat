@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  walletbackkt startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and WALLETBACKKT_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\walletbackkt-0.0.1.jar;%APP_HOME%\lib\ktor-server-tomcat-1.2.4.jar;%APP_HOME%\lib\ktor-auth-jwt-1.2.4.jar;%APP_HOME%\lib\ktor-auth-1.2.4.jar;%APP_HOME%\lib\ktor-gson-1.2.4.jar;%APP_HOME%\lib\ktor-server-servlet-1.2.4.jar;%APP_HOME%\lib\ktor-server-host-common-1.2.4.jar;%APP_HOME%\lib\ktor-server-core-1.2.4.jar;%APP_HOME%\lib\ktor-client-core-jvm-1.2.4.jar;%APP_HOME%\lib\ktor-http-cio-jvm-1.2.4.jar;%APP_HOME%\lib\ktor-http-jvm-1.2.4.jar;%APP_HOME%\lib\ktor-network-1.2.4.jar;%APP_HOME%\lib\ktor-utils-jvm-1.2.4.jar;%APP_HOME%\lib\kotlin-stdlib-jdk8-1.3.50.jar;%APP_HOME%\lib\logback-classic-1.2.1.jar;%APP_HOME%\lib\exposed-0.17.4.jar;%APP_HOME%\lib\postgresql-42.2.8.jar;%APP_HOME%\lib\HikariCP-3.4.1.jar;%APP_HOME%\lib\java-jwt-3.8.3.jar;%APP_HOME%\lib\kotlin-stdlib-jdk7-1.3.50.jar;%APP_HOME%\lib\kotlinx-coroutines-io-jvm-0.1.14.jar;%APP_HOME%\lib\kotlinx-io-jvm-0.1.14.jar;%APP_HOME%\lib\atomicfu-0.12.11.jar;%APP_HOME%\lib\kotlinx-coroutines-jdk8-1.3.0.jar;%APP_HOME%\lib\kotlinx-coroutines-core-1.3.0.jar;%APP_HOME%\lib\kotlin-reflect-1.3.50.jar;%APP_HOME%\lib\kotlin-stdlib-1.3.50.jar;%APP_HOME%\lib\slf4j-api-1.7.26.jar;%APP_HOME%\lib\config-1.3.1.jar;%APP_HOME%\lib\tomcat-catalina-9.0.10.jar;%APP_HOME%\lib\tomcat-embed-core-9.0.10.jar;%APP_HOME%\lib\logback-core-1.2.1.jar;%APP_HOME%\lib\json-simple-1.1.1.jar;%APP_HOME%\lib\jwks-rsa-0.7.0.jar;%APP_HOME%\lib\gson-2.8.5.jar;%APP_HOME%\lib\joda-time-2.10.2.jar;%APP_HOME%\lib\h2-1.4.199.jar;%APP_HOME%\lib\jackson-databind-2.10.0.pr3.jar;%APP_HOME%\lib\commons-codec-1.12.jar;%APP_HOME%\lib\kotlinx-coroutines-io-0.1.14.jar;%APP_HOME%\lib\kotlinx-io-0.1.14.jar;%APP_HOME%\lib\kotlinx-coroutines-core-common-1.3.0.jar;%APP_HOME%\lib\kotlin-stdlib-common-1.3.50.jar;%APP_HOME%\lib\annotations-13.0.jar;%APP_HOME%\lib\atomicfu-common-0.12.11.jar;%APP_HOME%\lib\tomcat-jsp-api-9.0.10.jar;%APP_HOME%\lib\tomcat-util-scan-9.0.10.jar;%APP_HOME%\lib\tomcat-api-9.0.10.jar;%APP_HOME%\lib\tomcat-coyote-9.0.10.jar;%APP_HOME%\lib\tomcat-servlet-api-9.0.10.jar;%APP_HOME%\lib\tomcat-util-9.0.10.jar;%APP_HOME%\lib\tomcat-juli-9.0.10.jar;%APP_HOME%\lib\tomcat-annotations-api-9.0.10.jar;%APP_HOME%\lib\tomcat-jni-9.0.10.jar;%APP_HOME%\lib\tomcat-jaspic-api-9.0.10.jar;%APP_HOME%\lib\commons-io-2.6.jar;%APP_HOME%\lib\guava-27.0-jre.jar;%APP_HOME%\lib\jackson-annotations-2.10.0.pr3.jar;%APP_HOME%\lib\jackson-core-2.10.0.pr3.jar;%APP_HOME%\lib\tomcat-el-api-9.0.10.jar;%APP_HOME%\lib\failureaccess-1.0.jar;%APP_HOME%\lib\listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar;%APP_HOME%\lib\jsr305-3.0.2.jar;%APP_HOME%\lib\checker-qual-2.5.2.jar;%APP_HOME%\lib\error_prone_annotations-2.2.0.jar;%APP_HOME%\lib\j2objc-annotations-1.1.jar;%APP_HOME%\lib\animal-sniffer-annotations-1.17.jar

@rem Execute walletbackkt
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %WALLETBACKKT_OPTS%  -classpath "%CLASSPATH%" io.ktor.server.tomcat.EngineMain %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable WALLETBACKKT_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%WALLETBACKKT_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
