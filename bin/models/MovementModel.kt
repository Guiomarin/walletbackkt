package com.uniajc.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Movements : IntIdTable() {
    val concept = varchar("concept", length = 45)
    val date = varchar("date", length = 45)
    val ammount = varchar("ammount", length = 45)
    val latitude = float("latitude")
    val longitude = float("longitude")
    val image = varchar("image", length = 50)
    val wallet = reference("wallet", Wallets)
}

class MovementDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<MovementDAO>(Movements)

    var concept by Movements.concept
    var date by Movements.date
    var ammount by Movements.ammount
    var latitude by Movements.latitude
    var longitude by Movements.longitude
    var image by Movements.image
    val comments by WalletDAO referrersOn Movements.wallet
}
