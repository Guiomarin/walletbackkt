package com.uniajc.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Wallets : IntIdTable() {
    val name = varchar("name", length = 45).uniqueIndex()
    val lastupdate = varchar("lastupdate", length = 45)
    val ammount = varchar("ammount", length = 45)
    val space = reference("user", Users)
}

class WalletDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<WalletDAO>(Wallets)

    var name by Wallets.name
    var lastupdate by Wallets.lastupdate
    var ammount by Wallets.ammount
    var space by UserDAO referencedOn Wallets.space
}