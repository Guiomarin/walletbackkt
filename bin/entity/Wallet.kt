package com.uniajc.entity

data class Wallet(
    val id: Int,
    val name: String,
    val lastupdate: String,
    val ammount: String,
    var user: User
)