package com.uniajc.entity

data class Movement(
    var id: Int = 0,
    var concept: String = "",
    var date: String = "",
    var ammount: String,
    var image: String,
    var latitude: Float = 0.0F,
    var longitude: Float = 0.0F,
    var wallet: Wallet
)