package com.uniajc.endpoints

import com.uniajc.auth.JWTBase
import com.uniajc.entity.User
import com.uniajc.repo.UserRepository
import com.uniajc.responses.Message
import io.ktor.application.call
import io.ktor.auth.UserPasswordCredential
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.post

fun Route.auth(userRepository: UserRepository) {

    post("login") {

        val credentials = call.receive<UserPasswordCredential>()
        val user = userRepository.findUserByCredentials(credentials)

        if (user == null) {
            call.respond(HttpStatusCode.NotFound, Message(message = "Credenciales inválidas"))
        } else {
            val token = JWTBase.makeToken(user)
            call.respond(HttpStatusCode.OK, mapOf("jwtKey" to token))
        }
    }

    post("register") {
        val user = call.receive<User>()
        val createdUser = userRepository.insertUser(user)

        if (createdUser == null) {
            call.respond(HttpStatusCode.NotAcceptable, Message(message = "No se pudo crear el usuario"))
        } else {
            call.respond(HttpStatusCode.OK, createdUser)
        }
    }
}