package com.uniajc.util

import com.uniajc.models.Users
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction


object DatabaseInit {
    fun init() {
        Database.connect(getConfigHikari())
        createTables()
    }

    private fun createTables() {
        transaction {
            SchemaUtils.create(Users)

            if (Users.selectAll().count() == 0) {
                Users.insert {
                    it[name] = "wallet"
                    it[username] = "admin"
                    it[password] = "admin"
                }
            }
        }
    }

    private fun getConfigHikari(): HikariDataSource {
        val config = HikariConfig()
        config.dataSourceClassName = "org.postgresql.ds.PGSimpleDataSource"
        config.jdbcUrl = "jdbc:/postgres://35.226.59.139:5432/wallet"
        config.username = "admin"
        config.password = "admin"
        config.maximumPoolSize = 3
        config.validate()

        return HikariDataSource(config)
    }
}